package com.project.base.gate.authentication.service;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.oauth2.core.AbstractOAuth2Token;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    public Mono<Authentication> getAuthentication() {
        return ReactiveSecurityContextHolder
            .getContext()
            .map(SecurityContext::getAuthentication)
            .map(authentication ->
                new org.springframework.security.core.Authentication() {

                    public List<String> getAdditionalInfoForAuthentication() {
                        return Collections.emptyList();
                    }

                    @Override
                    public String getName() {
                        return authentication.getName();
                    }

                    @Override
                    public Collection<? extends GrantedAuthority> getAuthorities() {
                        return authentication.getAuthorities();
                    }

                    @Override
                    public Object getCredentials() {
                        return authentication.getCredentials();
                    }

                    @Override
                    public Object getDetails() {
                        return authentication.getDetails();
                    }

                    @Override
                    public Object getPrincipal() {
                        return authentication.getPrincipal();
                    }

                    @Override
                    public boolean isAuthenticated() {
                        return authentication.isAuthenticated();
                    }

                    @Override
                    public void setAuthenticated(boolean b) throws IllegalArgumentException {

                    }

                });
    }

    public Mono<String> getAuthenticatedUserToken() {
        return getPrincipal().map(AbstractOAuth2Token::getTokenValue);
    }

    public Mono<String> getUserId() {
        return getPrincipal()
            .map(org.springframework.security.oauth2.jwt.Jwt::getClaims)
            .map(claims -> (String) claims.get("sub"));
    }

    private Mono<Jwt> getPrincipal() {
        return getAuthentication().map(authentication -> (Jwt) authentication.getPrincipal());
    }

}
