package com.project.base.gate.authentication.service;

import org.springframework.security.core.Authentication;
import reactor.core.publisher.Mono;

public interface AuthenticationService {

    Mono<Authentication> getAuthentication();

    Mono<String> getAuthenticatedUserToken();

    Mono<String> getUserId();

}
