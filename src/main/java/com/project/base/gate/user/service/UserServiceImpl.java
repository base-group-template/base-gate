package com.project.base.gate.user.service;

import com.project.base.gate.authentication.service.AuthenticationService;
import com.project.base.gate.user.domain.UserDto;
import com.project.base.gate.user.domain.UserRepresentationDto;
import com.project.base.gate.user.domain.requests.CreateUser;
import com.project.base.gate.user.domain.requests.DeleteUser;
import com.project.base.gate.user.domain.requests.GetUser;
import com.project.base.gate.user.domain.requests.UpdateUser;
import io.rsocket.metadata.WellKnownMimeType;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeType;
import org.springframework.util.MimeTypeUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class UserServiceImpl implements UserService {

    private final MimeType MESSAGE_RSOCKET_AUTHENTICATION = MimeTypeUtils.parseMimeType(WellKnownMimeType.MESSAGE_RSOCKET_AUTHENTICATION.getString());
    private final MimeType TEXT_PLAIN = MimeTypeUtils.parseMimeType(WellKnownMimeType.TEXT_PLAIN.getString());
    private final Mono<RSocketRequester> getRSocketUserServiceRequester;
    private final AuthenticationService authenticationService;

    public UserServiceImpl(Mono<RSocketRequester> getRSocketUserServiceRequester, AuthenticationService authenticationService) {
        this.getRSocketUserServiceRequester = getRSocketUserServiceRequester;
        this.authenticationService = authenticationService;
    }

    @Override
    public Flux<UserDto> getAllUsers() {
        return authenticationService.getAuthenticatedUserToken()
            .flatMapMany(token ->
                getRSocketUserServiceRequester.flatMapMany(
                    requester -> requester.route("user.getAllUsers")
                        .metadata(token, MESSAGE_RSOCKET_AUTHENTICATION)
                        .retrieveFlux(UserDto.class)
                )
            );
    }

    @Override
    public Mono<UserDto> createUser(UserDto userDto) {
        return authenticationService.getAuthenticatedUserToken()
            .flatMap(token ->
                getRSocketUserServiceRequester.flatMap(requester ->
                    requester.route("user.createUser")
                        .metadata(token, MESSAGE_RSOCKET_AUTHENTICATION)
                        .data(new CreateUser(userDto))
                        .retrieveMono(UserDto.class)
                )
            );
    }

    @Override
    public Mono<UserDto> getUser(String userId) {
        return authenticationService.getAuthenticatedUserToken()
            .flatMap(token ->
                getRSocketUserServiceRequester.flatMap(requester ->
                    requester.route("user.getUser")
                        .metadata(token, TEXT_PLAIN)
                        .data(new GetUser(userId))
                        .retrieveMono(UserDto.class)
                )
            );
    }

    @Override
    public Mono<UserDto> updateUser(String userId, UserDto userDto) {
        return authenticationService.getAuthenticatedUserToken()
            .flatMap(token ->
                getRSocketUserServiceRequester.flatMap(requester ->
                    requester.route("user.updateUser")
                        .metadata(token, MESSAGE_RSOCKET_AUTHENTICATION)
                        .data(new UpdateUser(userId, userDto))
                        .retrieveMono(UserDto.class)
                )
            );
    }

    @Override
    public Mono<Void> deleteUser(String userId) {
        return authenticationService.getAuthenticatedUserToken()
            .flatMap(token ->
                getRSocketUserServiceRequester.flatMap(requester ->
                    requester.route("user.deleteUser")
                        .metadata(token, MESSAGE_RSOCKET_AUTHENTICATION)
                        .data(new DeleteUser(userId))
                        .retrieveMono(Void.TYPE)
                )
            );
    }

    @Override
    public Flux<UserRepresentationDto> testIdentity() {
        return getRSocketUserServiceRequester.flatMapMany(requester ->
            requester.route("user.identityTest")
                .retrieveFlux(UserRepresentationDto.class)
        );
    }
}
