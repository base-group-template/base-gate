package com.project.base.gate.user.service;

import com.project.base.gate.user.domain.UserDto;
import com.project.base.gate.user.domain.UserRepresentationDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserService {
    Flux<UserDto> getAllUsers();

    Mono<UserDto> createUser(UserDto userDto);

    Mono<UserDto> getUser(String userId);

    Mono<UserDto> updateUser(String userId, UserDto userDto);

    Mono<Void> deleteUser(String userId);

    Flux<UserRepresentationDto> testIdentity();
}
