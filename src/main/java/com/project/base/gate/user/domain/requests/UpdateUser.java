package com.project.base.gate.user.domain.requests;

import com.project.base.gate.user.domain.UserDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateUser {
    private String userId;
    private UserDto userDto;
}
