package com.project.base.gate.user.domain;


public class CredentialRepresentationDto {
    private String type;
    private Boolean temporary;
    private String value;

    public CredentialRepresentationDto() {
    }

    public CredentialRepresentationDto(String type, Boolean temporary, String value) {
        this.type = type;
        this.temporary = temporary;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getTemporary() {
        return temporary;
    }

    public void setTemporary(Boolean temporary) {
        this.temporary = temporary;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "CredentialRepresentation{" +
            "type='" + type + '\'' +
            ", temporary=" + temporary +
            ", value='" + value + '\'' +
            '}';
    }
}
