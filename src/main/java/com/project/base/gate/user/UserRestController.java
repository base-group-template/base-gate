package com.project.base.gate.user;

import com.project.base.gate.core.configuration.exception.NameRequiredException;
import com.project.base.gate.user.domain.UserDto;
import com.project.base.gate.user.domain.UserRepresentationDto;
import com.project.base.gate.user.exception.UserNotFoundException;
import com.project.base.gate.user.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("user")
public class UserRestController {

    private final UserService userService;

    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("all")
    public Flux<UserDto> getAllUsers() {
        return userService.getAllUsers();
    }

    @PostMapping
    public Mono<UserDto> createUser(@RequestBody UserDto userDto) {
        return userService.createUser(userDto);
    }

    @GetMapping("{user-userId}")
    public Mono<UserDto> getUser(@PathVariable("user-userId") String userId) {
        return userService.getUser(userId)
            .switchIfEmpty(
                Mono.error(
                    new NameRequiredException(
                        HttpStatus.NOT_FOUND,
                        "The user " + userId + " was not found",
                        new UserNotFoundException()
                    )
                )
            );
    }

    @PutMapping("{user-userId}")
    public Mono<UserDto> updateUser(@PathVariable("user-userId") String userId, @RequestBody UserDto userDto) {
        return userService.updateUser(userId, userDto)
            .switchIfEmpty(
                Mono.error(
                    new NameRequiredException(
                        HttpStatus.NOT_FOUND,
                        "The user " + userId + " was not found",
                        new UserNotFoundException()
                    )
                )
            );
    }

    @DeleteMapping("{user-userId}")
    public Mono<Void> deleteUser(@PathVariable("user-userId") String userId) {
        return userService.deleteUser(userId);
    }

    @GetMapping("test-identity")
    public Flux<UserRepresentationDto> testIdentity() {
        return userService.testIdentity();
    }
}
