package com.project.base.gate.document.domain.requests

data class DeleteAllDocuments(val userId: String)