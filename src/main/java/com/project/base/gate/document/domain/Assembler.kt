package com.project.base.gate.document.domain

import org.apache.commons.io.IOUtils
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.core.io.buffer.DataBufferUtils
import org.springframework.http.codec.multipart.FilePart
import reactor.core.publisher.Mono
import java.io.InputStream
import java.util.*

fun FilePart.toDomain(userId: String): Mono<CreateDocumentDto> =
    this.toFileData()
        .map { data ->
            CreateDocumentDto(
                userId,
                CreateDocumentDto.Category.RECEIPT,
                data
            )
        }

private fun FilePart.toFileData(): Mono<FileDataDto> =
    DataBufferUtils.join(content()).map { dataBuffer ->
        FileDataDto(
            filename(),
            dataBuffer.toByteArray(),
            getType(filename()),
            headers().contentType!!.toString()
        )
    }

private fun getType(filename: String): FileDataDto.Extension =
    when (filename.substringAfterLast('.', "").lowercase(Locale.getDefault())) {
        "pdf" -> FileDataDto.Extension.PDF
        "jpg" -> FileDataDto.Extension.JPG
        "jpeg" -> FileDataDto.Extension.JPEG
        else -> FileDataDto.Extension.FALLBACK
    }

fun inputStreamToString(inputStream: InputStream): String = IOUtils.toString(inputStream, Charsets.UTF_8)

fun DataBuffer.toByteArray(): ByteArray {
    val bytes = ByteArray(this.readableByteCount())
    this.read(bytes)
    DataBufferUtils.release(this)
    return bytes
}
