package com.project.base.gate.document.domain.requests

import com.project.base.gate.document.domain.DocumentDto

data class SaveDocument(val userId: String, val data: DocumentDto.Data)