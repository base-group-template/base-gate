package com.project.base.gate.document.domain.requests

data class DeleteDocument(val userId: String, val documentId: String)