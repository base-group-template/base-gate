package com.project.base.gate.document.domain

data class CreateDocumentDto(
    var userId: String,
    var category: Category,
    var file: FileDataDto
) {
    enum class Category {
        RECEIPT
    }
}