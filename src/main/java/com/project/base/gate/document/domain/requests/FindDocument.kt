package com.project.base.gate.document.domain.requests

data class FindDocument(val userId: String, val documentId: String)