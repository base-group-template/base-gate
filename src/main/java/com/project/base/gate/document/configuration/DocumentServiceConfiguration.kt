package com.project.base.gate.document.configuration

import io.rsocket.core.RSocketConnector
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.messaging.rsocket.RSocketRequester
import reactor.core.publisher.Mono
import reactor.util.retry.Retry
import java.time.Duration

@Configuration
open class DocumentServiceConfiguration {

    @Value("\${services.document.host}")
    private var host: String = ""

    @Value("\${services.document.port}")
    private var port: Int = 0

    @Bean(name = ["getRSocketDocumentServiceRequester"])
    open fun getRSocketRequester(builder: RSocketRequester.Builder): Mono<RSocketRequester> {
        return Mono.just(
            builder
                .rsocketConnector { rSocketConnector: RSocketConnector ->
                    rSocketConnector.reconnect(
                        Retry.fixedDelay(
                            2,
                            Duration.ofSeconds(2)
                        )
                    )
                }
                .dataMimeType(MediaType.APPLICATION_CBOR)
                .tcp(host, port)
        )
    }
}