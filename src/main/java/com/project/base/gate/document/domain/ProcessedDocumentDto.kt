package com.project.base.gate.document.domain

data class ProcessedDocumentDto(
    val name: String
)