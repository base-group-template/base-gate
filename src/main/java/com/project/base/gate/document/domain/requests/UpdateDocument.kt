package com.project.base.gate.document.domain.requests

import com.project.base.gate.document.domain.DocumentDto

data class UpdateDocument(val userId: String, val documentId: String, val data: DocumentDto.Data)