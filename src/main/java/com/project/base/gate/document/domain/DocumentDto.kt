package com.project.base.gate.document.domain

import java.io.InputStream

data class DocumentDto(
    val id: String?,
    val userId: String,
    val type: DocumentClass,
    val fileId: String,
    var processedDocumentId: String
) {
    enum class DocumentClass {
        RECEIPT
    }

    data class Data(
        var name: String,
        var inputStream: InputStream,
        var type: Type
    )

    enum class Type(val extension: String) {
        PDF("pdf"),
        JPG("jpg"),
        JPEG("jpeg"),
        FALLBACK("*")
    }
}