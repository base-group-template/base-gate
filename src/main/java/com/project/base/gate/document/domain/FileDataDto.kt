package com.project.base.gate.document.domain

data class FileDataDto(
    var name: String,
    var bytes: ByteArray,
    var extension: Extension,
    var mediaType: String
) {

    enum class Extension(val extension: String) {
        PDF("pdf"),
        JPG("jpg"),
        JPEG("jpeg"),
        FALLBACK("*")
    }
}