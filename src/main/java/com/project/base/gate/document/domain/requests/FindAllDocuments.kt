package com.project.base.gate.document.domain.requests

data class FindAllDocuments(val userId: String)