package com.project.base.gate.document.service

import com.project.base.gate.document.domain.DocumentDto
import org.springframework.http.codec.multipart.FilePart
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface DocumentService {

    fun saveDocument(data: DocumentDto.Data): Mono<DocumentDto>

    fun findDocument(documentId: String): Mono<DocumentDto>

    fun findAllDocuments(): Flux<DocumentDto>

    fun updateDocument(documentId: String, data: DocumentDto.Data): Mono<DocumentDto>

    fun deleteDocument(documentId: String): Mono<Boolean>

    fun deleteAllDocuments(): Mono<Boolean>

    fun processFiles(fileParts: Flux<FilePart>): Flux<DocumentDto>
}