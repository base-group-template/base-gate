package com.project.base.gate.document.exception;

public class DocumentNotFoundException extends Exception {
    public DocumentNotFoundException() {
    }

    public DocumentNotFoundException(String message) {
        super(message);
    }
}
