package com.project.base.gate.document.domain.requests

import com.project.base.gate.document.domain.CreateDocumentDto

data class ProcessFile(
    val createDocument: CreateDocumentDto
)