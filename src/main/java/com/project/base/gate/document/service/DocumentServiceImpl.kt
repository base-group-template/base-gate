package com.project.base.gate.document.service

import com.project.base.gate.authentication.service.AuthenticationService
import com.project.base.gate.document.domain.DocumentDto
import com.project.base.gate.document.domain.requests.*
import com.project.base.gate.document.domain.toDomain
import io.rsocket.metadata.WellKnownMimeType
import org.springframework.http.codec.multipart.FilePart
import org.springframework.messaging.rsocket.RSocketRequester
import org.springframework.stereotype.Component
import org.springframework.util.MimeTypeUtils
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.util.function.Tuple2

@Component
class DocumentServiceImpl(
    private val getRSocketDocumentServiceRequester: Mono<RSocketRequester>,
    private val authenticationService: AuthenticationService
) : DocumentService {

    companion object {
        private val MESSAGE_RSOCKET_AUTHENTICATION =
            MimeTypeUtils.parseMimeType(WellKnownMimeType.MESSAGE_RSOCKET_AUTHENTICATION.string)
        private val TEXT_PLAIN = MimeTypeUtils.parseMimeType(WellKnownMimeType.TEXT_PLAIN.string)
    }

    override fun saveDocument(data: DocumentDto.Data): Mono<DocumentDto> =
        authenticationService.userId
            .flatMap { userId ->
                getRSocketDocumentServiceRequester.flatMap { requester: RSocketRequester ->
                    requester.route("document.saveDocument")
                        .data(
                            SaveDocument(
                                userId,
                                data
                            )
                        )
                        .retrieveMono(DocumentDto::class.java)
                }
            }

    override fun findDocument(documentId: String): Mono<DocumentDto> =
        authenticationService.userId
            .flatMap { userId ->
                getRSocketDocumentServiceRequester.flatMap { requester: RSocketRequester ->
                    requester.route("document.findDocument")
                        .data(
                            FindDocument(
                                userId,
                                documentId
                            )
                        )
                        .retrieveMono(DocumentDto::class.java)
                }
            }

    override fun findAllDocuments(): Flux<DocumentDto> =
        authenticationService.userId
            .flatMapMany { userId ->
                getRSocketDocumentServiceRequester.flatMapMany { requester: RSocketRequester ->
                    requester.route("document.findAllDocuments")
                        .data(FindAllDocuments(userId))
                        .retrieveFlux(DocumentDto::class.java)
                }
            }

    override fun updateDocument(documentId: String, data: DocumentDto.Data): Mono<DocumentDto> =
        authenticationService.userId
            .flatMap { userId ->
                getRSocketDocumentServiceRequester.flatMap { requester: RSocketRequester ->
                    requester.route("document.updateDocument")
                        .data(
                            UpdateDocument(
                                userId,
                                documentId,
                                data
                            )
                        )
                        .retrieveMono(DocumentDto::class.java)
                }
            }

    override fun deleteDocument(documentId: String): Mono<Boolean> =
        authenticationService.userId
            .flatMap { userId ->
                getRSocketDocumentServiceRequester.flatMap { requester: RSocketRequester ->
                    requester.route("document.deleteDocument")
                        .data(
                            DeleteDocument(
                                userId,
                                documentId
                            )
                        )
                        .retrieveMono(Boolean::class.java)
                }
            }

    override fun deleteAllDocuments(): Mono<Boolean> =
        authenticationService.userId
            .flatMap { userId ->
                getRSocketDocumentServiceRequester.flatMap { requester: RSocketRequester ->
                    requester.route("document.deleteAllDocuments")
                        .data(DeleteAllDocuments(userId))
                        .retrieveMono(Boolean::class.java)
                }
            }

    override fun processFiles(fileParts: Flux<FilePart>): Flux<DocumentDto> =
        userData()
            .flatMapMany {
                val userId = it.t1
                val token = it.t2
                fileParts.flatMap { filePart -> filePart.toDomain(userId) }
                    .collectList()
                    .flatMapMany { documents ->
                        getRSocketDocumentServiceRequester.flatMapMany { requester ->
                            requester.route("document.processFiles")
                                .metadata(token, TEXT_PLAIN)
                                .data(ProcessFiles(documents))
                                .retrieveFlux(DocumentDto::class.java)
                        }
                    }
            }

    private fun userData(): Mono<Tuple2<String, String>> =
        Mono.zip(
            authenticationService.userId,
            authenticationService.authenticatedUserToken
        )
}