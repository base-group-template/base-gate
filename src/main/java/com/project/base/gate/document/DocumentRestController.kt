package com.project.base.gate.document

import com.project.base.gate.document.domain.DocumentDto
import com.project.base.gate.document.service.DocumentService
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.codec.multipart.FilePart
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@RestController
@RequestMapping("document")
class DocumentRestController(private val documentService: DocumentService) {
    @PostMapping
    fun saveDocument(@RequestBody data: DocumentDto.Data): Mono<DocumentDto> =
        documentService.saveDocument(data)

    @GetMapping("{document-id}")
    fun findDocument(@PathVariable("document-id") documentId: String): Mono<DocumentDto> =
        documentService.findDocument(documentId)

    @GetMapping("all")
    fun findAllDocuments(): Flux<DocumentDto> =
        documentService.findAllDocuments()

    @PutMapping("{document-id}")
    fun updateDocument(
        @PathVariable("document-id") documentId: String,
        @RequestBody data: DocumentDto.Data
    ): Mono<DocumentDto> =
        documentService.updateDocument(documentId, data)

    @DeleteMapping("{document-id}")
    fun deleteDocument(@PathVariable("document-id") documentId: String): Mono<Boolean> =
        documentService.deleteDocument(documentId)

    @DeleteMapping("all")
    fun deleteAllDocuments(): Mono<Boolean> =
        documentService.deleteAllDocuments()

    @PostMapping(
        value = ["process-files"],
        consumes = [MediaType.MULTIPART_FORM_DATA_VALUE],
        produces = [MediaType.APPLICATION_NDJSON_VALUE]
    )
    @ResponseStatus(value = HttpStatus.OK)
    fun processFiles(@RequestPart("files") filesParts: Flux<FilePart>): Flux<DocumentDto> =
        documentService.processFiles(filesParts)
}