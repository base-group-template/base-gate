package com.project.base.gate.core.configuration.rsocket;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.kotlin.KotlinModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.codec.cbor.Jackson2CborDecoder;
import org.springframework.http.codec.cbor.Jackson2CborEncoder;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.messaging.rsocket.RSocketStrategies;

import java.util.List;

@Configuration
public class RSocketConfiguration {

    @Bean
    public RSocketStrategies rSocketStrategies() {

        ObjectMapper mapper = new ObjectMapper().registerModule(new KotlinModule());

        return RSocketStrategies.builder()
            .encoders(encoders ->
                encoders.addAll(
                    List.of(
                        new Jackson2CborEncoder(),
                        new Jackson2JsonEncoder(),
                        new Jackson2JsonEncoder(mapper)
                    )
                )
            )
            .decoders(decoders ->
                decoders.addAll(
                    List.of(
                        new Jackson2CborDecoder(),
                        new Jackson2JsonDecoder(),
                        new Jackson2JsonDecoder(mapper)
                    )
                )
            )
            .build();
    }

}