package com.project.base.gate;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class GateApplicationTests {

    @Test
    void contextLoads() {
    }

}
