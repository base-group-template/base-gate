import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "com.project.base.gate"
version = "0.0.1-SNAPSHOT"
description = "base-gate"
java.sourceCompatibility = JavaVersion.VERSION_17

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}

object Versions {
    const val springBoot = "2.7.5"
    const val springSecurityOauthClient = "5.7.3"
    const val springdocWebflux = "1.4.1"
    const val jacksonModuleKotlin = "2.9.8"
    const val commons = "2.4"
    const val kotlinStdlibJDK8 = "1.7.20"
    const val kotlinReflect = "1.7.20"
    const val mongoBson = "4.6.1"
    const val reactorTest = "3.4.23"
    const val lombok = "1.18.24"
}

plugins {
    java
    `maven-publish`
    id("org.springframework.boot") version "2.7.5"
    id("io.spring.dependency-management") version "1.0.15.RELEASE"
    kotlin("jvm") version "1.6.21"
    kotlin("plugin.spring") version "1.6.21"
}

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    compileOnly("org.projectlombok:lombok:${Versions.lombok}")

    implementation("org.springframework.boot:spring-boot-starter-parent:${Versions.springBoot}")
    implementation("org.springframework.boot:spring-boot-starter-actuator:${Versions.springBoot}")
    implementation("org.springframework.boot:spring-boot-starter-oauth2-resource-server:${Versions.springBoot}")
    implementation("org.springframework.boot:spring-boot-starter-webflux:${Versions.springBoot}")
    implementation("org.springframework.boot:spring-boot-starter-rsocket:${Versions.springBoot}")
    implementation("org.springframework.security:spring-security-oauth2-client:${Versions.springSecurityOauthClient}")
    implementation("org.springdoc:springdoc-openapi-webflux-ui:${Versions.springdocWebflux}")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:${Versions.jacksonModuleKotlin}")
    implementation("commons-io:commons-io:${Versions.commons}")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.kotlinStdlibJDK8}")
    implementation("org.jetbrains.kotlin:kotlin-reflect:${Versions.kotlinReflect}")
    implementation("org.mongodb:bson:${Versions.mongoBson}")

    annotationProcessor("org.projectlombok:lombok:${Versions.lombok}")

    testImplementation("org.springframework.boot:spring-boot-starter-test:${Versions.springBoot}")
    testImplementation("io.projectreactor:reactor-test:${Versions.reactorTest}")
    testImplementation("org.jetbrains.kotlin:kotlin-test:${Versions.kotlinStdlibJDK8}")
}

publishing {
    publications.create<MavenPublication>("maven") {
        from(components["java"])
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "17"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.named<Jar>("jar") {
    enabled = false
}
